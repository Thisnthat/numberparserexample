﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberParserExample.CommandLine.Supported
{
    public class FilePathArgument : SupportedArgument<FilePathArgument>
    {
        public FilePathArgument(string key, Action<FilePathArgument> callback) : base(key, callback)
        {

        }
        public FilePathArgument(string key, Action<FilePathArgument> callback, params string[] parameters) : base(key, callback)
        {
            this.Parameters.AddRange(parameters);
            this.HasPath = this.TrySetPath(parameters);
        }

        public string Path { get; private set; }
        public bool HasPath { get; private set; }

        private bool IsLegalPath(string path)
        {
            return Uri.IsWellFormedUriString(path, UriKind.RelativeOrAbsolute);
        }

        private bool TrySetPath(params string[] parameters)
        {
            if (parameters != null && parameters.Length > 0)
            {
                if (this.IsLegalPath(parameters[0]))
                {
                    this.Path = parameters[0];
                    return true;
                }
            }

            return false;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberParserExample.CommandLine.Supported
{
    public class SupportedArgument<T> : Argument, ISupportedArgument
    {
        public SupportedArgument(string key, Action<T> callback) : base(key)
        {
            this.Callback = callback;
        }

        public Action<T> Callback { get; set; }

        public void Invoke(params string[] parameters)
        {
            this.Callback.Invoke((T)Activator.CreateInstance(typeof(T), this.Key, this.Callback, parameters));
        }
    }
}

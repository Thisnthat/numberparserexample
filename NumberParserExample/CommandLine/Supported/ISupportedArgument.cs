﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberParserExample.CommandLine.Supported
{
    public interface ISupportedArgument
    {
        void Invoke(params string[] parameters);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberParserExample.CommandLine
{
    public class Argument
    {
        public Argument(string key) : this(key, null)
        {
            
        }
        public Argument(string key, params string[] parameters)
        {
            this.Parameters = new List<string>();

            this.Key = key;

            if (parameters != null && parameters.Length > 0)
            {
                this.Parameters.AddRange(parameters);
            }
        }

        public string Key { get; private set; }
        public List<string> Parameters { get; private set; }
    }
}

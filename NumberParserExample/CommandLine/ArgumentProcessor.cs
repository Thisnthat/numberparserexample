﻿using NumberParserExample.CommandLine.Supported;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberParserExample.CommandLine
{
    public class ArgumentProcessor
    {
        public ArgumentProcessor() : this(ArgumentProcessor.DefaultArgumentStartingCharacter)
        {

        }
        public ArgumentProcessor(char argStartingCharacter)
        {
            ArgParser = new ArugmentParser(argStartingCharacter);
        }

        public const char DefaultArgumentStartingCharacter = '-';

        private ArgumentMatcher ArgMatcher = new ArgumentMatcher();
        private ArugmentParser ArgParser;

        public void Add<T>(string key, Action<T> callback)
        {
            this.ArgMatcher.Add(new SupportedArgument<T>(key, callback));
        }
        public void Add(ISupportedArgument sArg)
        {
            this.ArgMatcher.Add(sArg);
        }

        public void ProcessAndInvoke(params string[] args)
        {
            this.ArgMatcher.MatchAndInvoke(this.ArgParser.Parse(args));
        }
    }
}

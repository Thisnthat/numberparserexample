﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberParserExample.CommandLine
{
    internal class ArugmentParser
    {
        internal ArugmentParser(char argStartChar)
        {
            this.ArgumentStartCharacter = argStartChar;
        }

        internal char ArgumentStartCharacter { get; private set; }

        internal List<Argument> Parse(string[] args)
        {
            List<Argument> parsedArgs = new List<Argument>();

            Argument current = null;
            foreach (string part in args)
            {
                if (part[0] == this.ArgumentStartCharacter)
                {
                    current = new Argument(part.Substring(1, part.Length - 1));
                    parsedArgs.Add(current);
                }
                else
                {
                    if (current != null)
                    {
                        current.Parameters.Add(part);
                    }
                }
            }

            return parsedArgs;
        }
    }
}

﻿using NumberParserExample.CommandLine.Supported;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberParserExample.CommandLine
{
    internal class ArgumentMatcher
    {
        internal ArgumentMatcher()
        {

        }

        private List<ISupportedArgument> SupportedArguments = new List<ISupportedArgument>();

        internal void Add<T>(string key, Action<T> callback)
        {
            this.SupportedArguments.Add(new SupportedArgument<T>(key, callback));
        }
        internal void Add(ISupportedArgument sArg)
        {
            this.SupportedArguments.Add(sArg);
        }

        internal void MatchAndInvoke(List<Argument> args)
        {
            foreach (Argument arg in args)
            {
                foreach (ISupportedArgument supportedArg in this.SupportedArguments)
                {
                    if (((Argument)supportedArg).Key == arg.Key)
                    {
                        supportedArg.Invoke(arg.Parameters.ToArray());
                    }
                }
            }
        }
    }
}

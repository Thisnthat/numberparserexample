﻿using NumberParserExample.CommandLine.Supported;
using NumberParserExample.Input;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberParserExample
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Hex/Dec converter";
            Console.WriteLine();

            ArgProcessor.Add<FilePathArgument>("i", ProcessInputFileArgument);
            ArgProcessor.Add<FilePathArgument>("o", ProcessOutputFileArgument);
            ArgProcessor.ProcessAndInvoke(args);

            string data = string.Empty;

            if (InputPathArgument != null && InputPathArgument.HasPath)
            {
                data = GetNumbersFromFileInput(InputPathArgument.Path);
                Console.WriteLine("Data read from file '{0}'", InputPathArgument.Path);
            }
            else
            {
                data = GetNumbersFromConsoleInput();
            }

            if (!string.IsNullOrEmpty(data))
            {
                Console.WriteLine("Parsing data...");
                WriteParsedNumbers(NumParser.Parse(data));
            }

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey(true);
        }

        private static char[] NumberSeparators = new char[] { ' ', ';', ',', '\n' };
        private static FilePathArgument OutputPathArgument;
        private static FilePathArgument InputPathArgument;

        static CommandLine.ArgumentProcessor ArgProcessor = new CommandLine.ArgumentProcessor();
        static NumberParser NumParser = new NumberParser(Program.NumberSeparators);

        static bool ValidateFilePathArgument(FilePathArgument fpArg, bool validateExistence)
        {
            if (!fpArg.HasPath)
            {
                WriteLineColored("File path is not formatted properly!", ConsoleColor.Red);
                return false;
            }

            if (validateExistence && !File.Exists(fpArg.Path))
            {
                WriteLineColored("File not found!", ConsoleColor.Red);
                return false;
            }

            return true;
        }

        static void ProcessInputFileArgument(FilePathArgument filePathArg)
        {
            if (ValidateFilePathArgument(filePathArg, true))
            {
                InputPathArgument = filePathArg;
            }
        }

        static void ProcessOutputFileArgument(FilePathArgument filePathArg)
        {
            if (ValidateFilePathArgument(filePathArg, false))
            {
                OutputPathArgument = filePathArg;
            }
        }

        static void WriteLineColored(string line, ConsoleColor color)
        {
            ConsoleColor startingColor = Console.ForegroundColor;

            Console.ForegroundColor = color;
            Console.WriteLine(line);
            Console.ForegroundColor = startingColor;
        }

        static string GetNumbersFromConsoleInput()
        {
            string userInput = string.Empty;

            while (true)
            {
                Console.Clear();
                Console.WriteLine("Type your numbers:");
                Console.WriteLine();

                userInput += Console.ReadLine();

                Console.WriteLine("Continue entering numbers [Y/N]?");
                ConsoleKeyInfo kInfo = Console.ReadKey(true);

                if (kInfo.Key == ConsoleKey.N)
                {
                    break;
                }

                userInput += " ";
            }

            Console.Clear();

            return userInput;
        }

        static string GetNumbersFromFileInput(string path)
        {
            return File.ReadAllText(path);
        }

        static string FormatParsedNumbers(List<ParsedNumber> parsedNumbers)
        {
            StringBuilder sb = new StringBuilder();

            int largestNumber = parsedNumbers.Max(o => o.Number);
            int columnSpaces = (largestNumber.ToString().Length) + 2; // +2 for 0x prefix

            string header = string.Format("Input{0}|{0}Output", new string(' ', columnSpaces));
            string lineBreak = string.Format("-----{0}|{0}------", new string('-', columnSpaces));

            string leftPadding = new string(' ', (Console.BufferWidth / 2) - (header.Length / 2));

            sb.AppendLine();
            sb.AppendLine(leftPadding + header);
            sb.AppendLine(leftPadding + lineBreak);

            foreach (ParsedNumber pn in parsedNumbers)
            {
                string input = pn.ToString();
                string output = (pn.IsHex) ? pn.Number.ToString() : "0x" + pn.Number.ToString("X");

                string line = string.Format("{0}{1}|{2}{3}", input, new string(' ', columnSpaces + (columnSpaces - input.Length)), new string(' ', columnSpaces), output);
                sb.AppendLine(leftPadding + line);
            }

            return sb.ToString();
        }

        static void WriteParsedNumbers(List<ParsedNumber> parsedNumbers)
        {
            string output = FormatParsedNumbers(parsedNumbers);

            if (OutputPathArgument != null)
            {
                if (File.Exists(OutputPathArgument.Path))
                {
                    WriteLineColored(string.Format("The output file '{0}' already exists!", OutputPathArgument.Path), ConsoleColor.Red);
                }
                else
                {
                    StreamWriter sw = File.AppendText(OutputPathArgument.Path);
                    sw.Write(output);
                    sw.Close();
                }
            }

            Console.Write(output);
        }
    }
}

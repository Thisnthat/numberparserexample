﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberParserExample.Input
{
    public class NumberParser
    {
        public NumberParser(params char[] separatorCharacter)
        {
            this.Separators = new List<char>();

            if (separatorCharacter != null && separatorCharacter.Length > 0)
            {
                this.Separators.AddRange(separatorCharacter);
            }
        }

        public List<char> Separators { get; private set; }

        public List<ParsedNumber> Parse(string input)
        {
            List<ParsedNumber> parsedNumbers = new List<ParsedNumber>();

            string[] splitInput = input.Split(this.Separators.ToArray());

            foreach (string item in splitInput)
            {
                if (!string.IsNullOrEmpty(item))
                {
                    bool isHex;
                    int number;
                    if (this.TryGetNumber(item, out number, out isHex))
                    {
                        parsedNumbers.Add(new ParsedNumber(number, isHex));
                    }
                }
            }

            return parsedNumbers;
        }

        private bool TryGetNumber(string input, out int number, out bool isHex)
        {
            isHex = input.StartsWith("0x");
            input = (isHex) ? input.Substring(2) : input;
            return int.TryParse(input, (isHex) ? NumberStyles.HexNumber : NumberStyles.Integer, CultureInfo.InvariantCulture.NumberFormat, out number);
        }
    }
}

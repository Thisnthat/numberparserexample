﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumberParserExample.Input
{
    public class ParsedNumber
    {
        public ParsedNumber(int number, bool isHex)
        {
            this.Number = number;
            this.IsHex = isHex;
        }

        public int Number { get; private set; }
        public bool IsHex { get; private set; }

        public override string ToString()
        {
            return (this.IsHex) ? ("0x" + this.Number.ToString("X")) : this.Number.ToString();
        }
    }
}
